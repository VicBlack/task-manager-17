package ru.t1.kupriyanov.tm.command.task;

import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(projectId, taskId);
    }

    @Override
    public String getName() {
        return "task-unbind-from-project";
    }

    @Override
    public String getDescription() {
        return "Unbind task from project";
    }

}
