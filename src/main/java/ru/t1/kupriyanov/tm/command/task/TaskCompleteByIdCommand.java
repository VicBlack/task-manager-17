package ru.t1.kupriyanov.tm.command.task;

import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatusById(id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

}
